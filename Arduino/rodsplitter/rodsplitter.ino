
// Definition of interrupt names
// ISR interrupt service routine

#include <avr/io.h>
#include <avr/interrupt.h>

volatile int pulse_counter=0;
volatile int curState = 0;
int pulseSum;


void setup() {
  Serial.begin(9600);
  pinMode(2,INPUT);
  pinMode(13,OUTPUT);
 digitalWrite(2,HIGH);
  Serial.println("Initializing timerinterrupt");
  attachInterrupt(0,line1crossed,RISING);
  sei();
}

void loop() 
{ 
if (pulse_counter>= 1)
{
  pulse_counter = 0;
  if(curState == 0)
  {
  digitalWrite(13, HIGH);
  curState = 1;
  }
  else
  {
    digitalWrite(13, LOW);
    curState = 0;
  }
}
}

void line1crossed()
{

 pulse_counter++;
}

