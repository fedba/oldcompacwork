
// Definition of interrupt names
// ISR interrupt service routine

#include <avr/io.h>
#include <avr/interrupt.h>



#define INIT_TIMER_COUNT 0
#define RESET_TIMER2 TCNT2 = INIT_TIMER_COUNT
            

// Define Pins
#define inputPin0 22
#define inputPin1 23

volatile int int_counter, lineOne, lineTwo, signalready,lineOneSmall, lineTwoSmall,big,small;
int pulseSum;


// Aruino runs at 16 Mhz, so we have 61 Overflows per second...
// 1/ ((16000000 / 1024) / 256) = 1 / 61
ISR(TIMER2_OVF_vect) {
  int_counter += 1; //increment overflow counter
};

void setup() {
  Serial.begin(9600);
  Serial.println("Initializing timerinterrupt");
  attachInterrupt(0,line1crossed,RISING);
    attachInterrupt(1,line2crossed,RISING);
  //Timer2 Settings:  Timer Prescaler /1024
  TCCR2B |= ((1 << CS22) | (1 << CS21) | (1 << CS20));
  //Timer2 Overflow Interrupt Enable
  TIMSK2 |= (1 << TOIE2);
  //RESET_TIMER2;
  sei();
}

void loop() 
{ 
if(signalready)
{
  lineTwo = lineTwo - lineOne;  //get difference between two times
  lineTwoSmall = lineTwoSmall - lineOneSmall;
Serial.print(lineTwo);
Serial.print(", ");
Serial.println(lineTwoSmall);
  signalready = 0;
}
}

void line1crossed()
{
  cli();
lineOne = int_counter;    
lineOneSmall = TCNT2;
sei();
reti();
}

void line2crossed()
{
    cli();
lineTwo = int_counter;
lineTwoSmall = TCNT2;
//int_counter =0;
signalready =1;
sei();
reti();
}
