/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 13;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
pinMode(10, OUTPUT);  
  Serial.begin(9600);
}
int serialbyte;
// the loop routine runs over and over again forever:
void loop() {
  if (Serial.available() > 0) {
    serialbyte = Serial.read();

 if(serialbyte == 1){
    digitalWrite(led, HIGH); 
  delay(100);
 }else if (serialbyte == 2){
   digitalWrite(10, HIGH); 
  delay(100);
  }
  }else{
    digitalWrite(led,LOW);
    digitalWrite(10, LOW);
}
  }
