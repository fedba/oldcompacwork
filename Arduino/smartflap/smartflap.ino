
// Definition of interrupt names
// ISR interrupt service routine

#include <avr/io.h>
#include <avr/interrupt.h>

#define INIT_TIMER_COUNT 0
#define RESET_TIMER2 TCNT2 = INIT_TIMER_COUNT
#define first 10
#define second 11
#define third 12
            
volatile int curtainArray[5];
volatile int fruitPresent, interuptScalar=0;



// Aruino runs at 16 Mhz, so we have 61 Overflows per second...
// 1/ ((16000000 / 1024) / 256) = 1 / 61
ISR(TIMER2_OVF_vect) {
  if (interuptScalar >= 10)
  {
  curtainArray[4] = curtainArray[3];
  curtainArray[3] = curtainArray[2];
  curtainArray[2] = curtainArray[1];
  curtainArray[1] = curtainArray[0];
  curtainArray[0] = fruitPresent;
  fruitPresent = 0;
  }else{
    interuptScalar++;
  }
};

void setup() {
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  pinMode(7,INPUT);
  
  pinMode(8,INPUT);
  pinMode(9,INPUT);
  pinMode(first,INPUT);
  pinMode(second,INPUT);
  pinMode(third,INPUT);
  

//  Serial.begin(9600);
//  Serial.println("Initializing");
  attachInterrupt(0,proxSensor,RISING);
//  Timer2 Settings:  Timer Prescaler /1024
  TCCR2B |= ((1 << CS22) | (1 << CS21) | (1 << CS20));
//  Timer2 Overflow Interrupt Enable
  TIMSK2 |= (1 << TOIE2);
  RESET_TIMER2;
  sei();
}

void loop() 
{
if(curtainArray[4] == 1){
  digitalWrite(third,HIGH);
}
if(curtainArray[3] == 1){
  digitalWrite(second,HIGH);
}
if(curtainArray[2] == 1){
  digitalWrite(first,HIGH);
}

  
}

void proxSensor()
{
  fruitPresent =1;
} 
