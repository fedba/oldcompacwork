
// Definition of interrupt names
// ISR interrupt service routine

#include <avr/io.h>
#include <avr/interrupt.h>



#define INIT_TIMER_COUNT 0
#define RESET_TIMER2 TCNT2 = INIT_TIMER_COUNT
            
#define solenoid1 13
#define solenoid2 12
#define solenoid3 11
#define solenoid4 10



volatile int int_counter = 0;
int count = 0;
double rodCount =0;
volatile int switchOff =50;
volatile bool toggle = 0;
int toggle2 =0;

// Aruino runs at 16 Mhz, so we have 61 Overflows per second...
// 1/ ((16000000 / 1024) / 256) = 1 / 61
ISR(TIMER2_OVF_vect) {
  int_counter += 1;
};

void setup() {
    pinMode(solenoid1,OUTPUT); 
      pinMode(solenoid2,OUTPUT); 
        pinMode(solenoid3,OUTPUT); 
          pinMode(solenoid4,OUTPUT); 
          pinMode(4,OUTPUT);
 // Serial.begin(9600);
 // Serial.println("Initializing timerinterrupt");
  attachInterrupt(0,rodPulse,RISING);
  //Timer2 Settings:  Timer Prescaler /1024
  //TCCR2 |= ((1 < < CS22) | (1 << CS21) | (1 << CS20));
  //Timer2 Overflow Interrupt Enable
  TIMSK2 |= (1 << TOIE2);
  RESET_TIMER2;
  sei();
}

void loop() 
{


  
  if(int_counter == switchOff)
  {
 //   Serial.println("Off");
    if(toggle == 0)
  {
  digitalWrite(solenoid1,LOW);
  digitalWrite(solenoid2,LOW);
  delay(200);
  digitalWrite(4,HIGH);
  }else{
  digitalWrite(solenoid3,LOW);
  digitalWrite(solenoid4,LOW);
  delay(200);
  digitalWrite(4,HIGH);
  }
  }
}

void rodPulse()
{
   digitalWrite(4,LOW);
 delay(200);
//  Serial.println("Interrupt");
  rodCount = int_counter;
 //   Serial.println(rodCount);
  int_counter = 0;
  if(toggle == 1)
  {
  digitalWrite(solenoid1,HIGH);
 // Serial.println("High");
  digitalWrite(solenoid2,HIGH);
  toggle = 0;
  }else{
  digitalWrite(solenoid3,HIGH);
  digitalWrite(solenoid4,HIGH);
  toggle = 1;
  }
 // Serial.println(toggle);
  switchOff = (rodCount/16)*13;
     
sei();
//Serial.println("return");
}
