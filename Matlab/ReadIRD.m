[vidname,pathname] = uigetfile( ...
    {'*.avi;*.mpg;*.mpeg;*.mov','Video Files (*.avi,*.mov,*.mpeg)';
     '*.*',  'All Files (*.*)'}, ...
     'Select a video file');
 cd('./Damage');
 APPLEDAM = csvread('APPLE.csv');
 BELLDAM = csvread('BELLPEPR.csv');
 ONIONDAM = csvread('ONION.csv');
 POTATODAM = csvread('POTATO.csv');
 cd('../');
 
[pathstr, name, ext] = fileparts(strcat(pathname,vidname));

irdMov = VideoReader(strcat(pathname,vidname));

[irdData,IRDpath] = uigetfile('*.csv');
irdData = dataset('XLSFile',sprintf('%s\%s', IRDpath,irdData));

recordedRate = input('What is the recorded frame rate');
syncThump = input('Which impact is the sync thump');
syncFrame = input('Which Frame is the sync thump');
forRev = input('is it at the start or end(start = 0, end = 1)');

nFrames = irdMov.NumberOfFrames;
vidHeight = irdMov.Height;
vidWidth = irdMov.Width;
[m,n] = size(irdData);  % get ird data size
frameDiff = zeros(1,m);


t1 = APPLEDAM(:,1);  %# Time-stamps for file 1
d1 = APPLEDAM(:,2);  %# First column of data for file 1
plot(t1,d1,'r-');    %# Plot data from file 1, using a red line with circles
hold on;
t2 = ONIONDAM(:,1);  %# Time-stamps for file 2
d2 = ONIONDAM(:,2);  %# First column of data for file 2
plot(t2,d2,'g-');    %# Plot data from file 2, using a blue line with circles
t3 = BELLDAM(:,1);  %# Time-stamps for file 1
d3 = BELLDAM(:,2);  %# First column of data for file 1
plot(t3,d3,'c-');    %# Plot data from file 1, using a red line with circles
t4 = POTATODAM(:,1);  %# Time-stamps for file 1
d4 = POTATODAM(:,2);  %# First column of data for file 1
plot(t4,d4,'m-');    %# Plot data from file 1, using a red line with circles

labels = cellstr( num2str([1:m]') );
scatter(irdData.velChg2, irdData.maxG,'bx');
text(irdData.velChg2,irdData.maxG, labels);
xlabel('Velocity Change(m/sec)');
ylabel('Max G');
                         
legend('Apple Damage','Onion Damage','Bell Pepper Damage','Potato Damage','Damage');
saveas(gcf,strcat(pathname,name,'plot.jpg'));
hold off;
%for loop to calculate ird frame values
for k = 2 : m
    frameDiff(k) = (irdData.elapseTime(k)-irdData.elapseTime(k-1))*recordedRate;
end

%modify calculation forward and reverse for frame values

if forRev == 0
    displayframes(1) = syncFrame;
    for k = syncThump+1 : m
    displayframes(k) =  frameDiff(k)+ displayframes(k-1);
    end
   
end

if forRev == 1
    displayframes(syncThump) = syncFrame;
    for k = syncThump :-1: 2
    displayframes(k-1) = displayframes(k) - frameDiff(k) ;
    end
   
end

[l,w] = size(displayframes);

% Preallocate movie structure.
mov(1:(w*10)) = ...
    struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'),...
           'colormap', []);

       currentFrame = 1;
       frameCount = 1;
% Read one frame at a time.
hf = figure;
set(hf, 'position', [150 150 vidWidth vidHeight])

for k = 1 : m-1
    currentFrame = displayframes(k);
    for frame = currentFrame-5 : currentFrame+5
        if ((frame < nFrames)&& (frame >0));         
            %add text to image
            subplot('Position', [0 0 1 1]);
            image(read(irdMov, frame));
            axis off
            truesize;
            text(10,50,sprintf('Frame = %d\nImpact number = %d',frame,k),'color','r');
        mov(frameCount) = getframe(2);
        frameCount = frameCount+1;
        end
    end

end

% % Size a figure based on the video's width and height.
% hf = figure;
% set(hf, 'position', [150 150 vidWidth vidHeight])
% 
% % Play back the movie once at the video's frame rate.
% movie(hf, mov, 1, 4);

writerObj = VideoWriter(strcat(pathname,name,'damage'));
open(writerObj);
writeVideo(writerObj,mov);
close(writerObj);
close all;
